﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPattern.Business
{
    class Tax: ITax
    {
        public decimal GetTax(Customer customer, decimal subTotal)
        {
            decimal tax = 0;
            switch (customer.StateCode)
            {
                case "TX":
                    tax = 0.08M * subTotal;
                    break;
                case "FL":
                    tax = 0.09M * subTotal;
                    break;
                default:
                    tax = 0.03M * subTotal;
                    break;
            }
            return tax;
        }
    }
}
