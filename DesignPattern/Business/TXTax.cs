﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPattern.Business
{
    class TXTax: ITXTax
    {
        public decimal GetTax(decimal subTotal)
        {
            return subTotal * 0.08M;
        }
    }
}
