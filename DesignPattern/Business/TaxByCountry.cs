﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPattern.Business
{
    class TaxByCountry: ITax
    {
        public decimal GetTax(Customer customer, decimal subTotal)
        {
            decimal tax = 0;
            switch (customer.Country)
            {
                case "US":
                    tax = 0.085M * subTotal;
                    break;
                case "VN":
                    tax = 0.095M * subTotal;
                    break;
                default:
                    tax = 0.03M * subTotal;
                    break;
            }
            return tax;
        }
    }
}
