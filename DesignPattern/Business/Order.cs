﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPattern.Business
{
    class Order: IOrder
    {
        public List<OrderLineItem> LineItems { get; set; }

        public ITaxFactory TaxFactory { get; set; }

        public Order() : this(new TxTaxFactory(new TaxFactory()))
        {

        }

        public Order(Customer customer) : this(new DateBaseTaxFactory(customer, new CustomerBaseTaxFactory(customer)))
        {

        }

        public Order(ITaxFactory taxFactory)
        {
            TaxFactory = taxFactory;
        }

        public decimal CalculateTotalCost(Customer customer)
        {
            decimal subTotal = 0;
            foreach (var item in LineItems)
            {
                subTotal += item.UnitPrice * item.Quantity;
            }

            var tax = TaxFactory.Tax.GetTax(customer, subTotal);
            return subTotal + tax;
        }

        public decimal CalculateTotalCost()
        {
            decimal subTotal = 0;
            foreach (var item in LineItems)
            {
                subTotal += item.UnitPrice * item.Quantity;
            }

            var txTaxFactory = (TxTaxFactory)(TaxFactory);
            var tax = txTaxFactory.TxTax.GetTax(subTotal);
            return subTotal + tax;
        }
    }
}
