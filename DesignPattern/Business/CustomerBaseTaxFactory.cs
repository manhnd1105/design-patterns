﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPattern.Business
{
    class CustomerBaseTaxFactory: ITaxFactory
    {
        public ITax Tax { get; set; }

        public CustomerBaseTaxFactory(Customer customer)
        {
            if (customer.Country != null)
            {
                Tax = new TaxByCountry();
            }
            else
            {
                Tax = new Tax();
            }
        }
    }
}
