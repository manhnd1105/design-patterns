﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPattern.Business
{
    class TaxZero: ITax
    {
        public decimal GetTax(Customer customer, decimal subTotal)
        {
            return 0.00M;
        }
    }
}
