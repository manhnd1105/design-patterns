﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPattern.Business
{
    class OrderLineItem
    {
        public string Code { get; set; }
        public decimal UnitPrice { get; set; }
        public int Quantity { get; set; }
    }
}
