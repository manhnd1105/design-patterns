﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPattern.Business
{
    class DateBaseTaxFactory: ITaxFactory
    {
        public ITax Tax { get; set; }
        public ITaxFactory TaxFactory { get; set; }
        public DateBaseTaxFactory(Customer customer, ITaxFactory taxFactory)
        {
            if (DateTime.Now.Day == 3 && DateTime.Now.Month == 6 && customer.StateCode == "TX")
            {
                Tax = new TaxZero();
            }
            else
            {
                Tax = TaxFactory.Tax;
            }
        }
    }
}
