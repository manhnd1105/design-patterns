﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPattern.Business
{
    public interface ITXTax
    {
        decimal GetTax(decimal subTotal);
    }
}
