﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPattern.Business
{
    public class Customer
    {
        public string Name { get; set; }
        public string StateCode { get; set; }
        public string Country { get; set; }
    }
}
