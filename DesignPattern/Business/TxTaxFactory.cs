﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPattern.Business
{
    public class TxTaxFactory: ITaxFactory
    {
        public ITax Tax { get; set; }
        public ITXTax TxTax { get; set; }
        public ITaxFactory TaxFactory { get; set; }
        public TxTaxFactory(ITaxFactory taxFactory)
        {
            TaxFactory = taxFactory;
            TxTax = new TXTax();
        }
    }
}
