﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPattern.Business
{
    class TaxFactory: ITaxFactory
    {
        public ITax Tax { get; set; }

        public TaxFactory()
        {
            Tax = new Tax();
        }
    }
}
