﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPattern.Business
{
    public interface ITax
    {
        decimal GetTax(Customer customer, decimal subTotal);

    }
}
