﻿using System.Collections.Generic;
using DesignPattern.Business;
using System;
namespace DesignPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            //Setup an order with sample data
            var order = new Order();
            order.LineItems = new List<OrderLineItem>()
            {
                new OrderLineItem(){ Code = "LineItem1", Quantity = 3, UnitPrice = 5.95M},
                new OrderLineItem(){ Code = "LineItem2", Quantity = 1, UnitPrice = 6.5M},
                new OrderLineItem(){ Code = "LineItem3", Quantity = 4, UnitPrice = 7.3M}
            };

            //Setup customers with sample data
            var customers = new List<Customer>()
            {            
                new Customer() { Name = "Customer1", Country = "US", StateCode = "TX" },
                new Customer() { Name = "Customer2", Country = "VN", StateCode = "FL" }
            };


            //Do business flow: calculate order total cost for customer
            foreach (var customer in customers)
            {
                //order.TaxFactory = new CustomerBaseTaxFactory(customer);
                var total = order.CalculateTotalCost();
                Console.WriteLine(String.Format("Total cost for {0} is {1}", customer.Name, total.ToString()));
            }
            Console.ReadKey();
        }
    }
}
